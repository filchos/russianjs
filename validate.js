import parseRuDictFile from './src/parsefile.js'

const dictPath = 'words.txt'

parseRuDictFile(dictPath).then(
  dict => {
    console.log('Parsed', dict.length, 'words')
  }
)
