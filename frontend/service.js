/* global fetch location */

import { queryFilter } from '../../src/filter.js'
import { Box, LocalStorageDriver } from '../../src/cardbox.js'

const DictService = {}

let dict

DictService.get = async () => {
  if (!dict) {
    const resp = await fetch('/words.txt')
    const text = await resp.text()
    const lines = text.split('\n')

    const dictparser = await import('../src/dictparser.js')

    const rdp = new dictparser.RuDictLineParser()
    for (const line of lines) {
      rdp.dispatch(line)
    }
    dict = rdp.finalize()
    dict = mergeExternalesIntoDict(dict)
  }
  return dict
}

const mergeExternalesIntoDict = async (dict) => {
  const path = '/externals.json'
  const res = await fetch(path)
  if (!res.ok) {
    console.warn('Loading', path, 'results in status', res.status)
    return
  }
  const externals = await res.json()
  for (const [key, ext] of Object.entries(externals)) {
    const stub = { key }
    if (dict.has(stub)) {
      dict.get(stub).ru.ext = ext
    } else {
      console.warn(key, 'not found')
    }
  }
  return dict
}

const FilterService = {}

FilterService.byCurrentURL = (dict) => {
  const url = new URL(location.href)
  const filter = queryFilter(url.searchParams)
  dict = dict.filter(filter)

  const boxName = url.searchParams.get('box')
  if (boxName) {
    const box = new Box(boxName)
    box.driver = new LocalStorageDriver('cardbox-')
    box.load()
    const boxKeys = box.keys
    dict = dict.filter(voc => boxKeys.has(voc.key))
  }

  return dict
}

FilterService.getURLParams = () => {
  const url = new URL(location.href)
  const params = {}
  for (const [key, value] of url.searchParams.entries()) {
    params[key] = value
  }
  return params
}

export { DictService, FilterService }
