import props from './props.js'

class RuleSet {
  constructor () {
    this.rules = []
    this.currentCondition = null
  }

  on (postfixes) {
    if (this.currentCondition) {
      throw new Error('Unresolved on')
    }
    this.currentCondition = postfixes
    return this
  }

  map (callback) {
    if (!this.currentCondition) {
      throw new Error('Missing on')
    }
    const pattern = this.currentCondition
    this.rules.push({ pattern, callback })
    this.currentCondition = null
    return this
  }

  return (returnStr) {
    const callback = (str) => returnStr
    return this.map(callback)
  }

  suffix (deleteCount, appendStr) {
    const callback = (str) => {
      if (deleteCount) {
        return str.slice(0, -deleteCount) + appendStr
      } else {
        return str + appendStr
      }
    }
    return this.map(callback)
  }

  resolve (term) {
    if (this.currentCondition) {
      throw new Error('Unresolved on')
    }
    const str = term.content
    for (const rule of this.rules) {
      if (rule.pattern.test(str)) {
        return rule.callback(str)
      }
    }
    return null
  }
}

RuleSet.make = () => new RuleSet()

class NounFormGuess {
  constructor (voc) {
    this.voc = voc
  }

  isValidBasicForm () {
    const p = this.voc.props
    return p.case === props.Nominative && p.number === props.Singular && p.animacy === props.Inanimate
  }

  get gender () {
    if (!this.isValidBasicForm()) {
      return false
    }

    return GenderRuleSet.resolve(this.voc.ru)
  }

  // see http://www.russianlessons.net/lessons/lesson11_main.php
  get plural () {
    if (!this.isValidBasicForm()) {
      return false
    }

    const gender = this.voc.props.gender ?? this.gender

    switch (gender) {
      case props.Masculine:
        return MasculinePluralRuleSet.resolve(this.voc.ru)
      case props.Feminine:
        return FemininePluralRuleSet.resolve(this.voc.ru)
      case props.Neuter:
        return NeuterPluralRuleSet.resolve(this.voc.ru)
      default:
        return null
    }
  }
}

const consonants = /[бвгджзйклмнпрстфхцчшщ]$/

const GenderRuleSet = RuleSet.make()
  .on(/[жчшщ]ь$/).return(props.Feminine)
  .on(/(ие|мя)$/).return(props.Neuter)
  .on(consonants).return(props.Masculine)
  .on(/[ая]$/).return(props.Feminine)
  .on(/[еоё]$/).return(props.Neuter)

const MasculinePluralRuleSet = RuleSet.make()
  .on(consonants).suffix(0, 'ы')
  .on(/й$/).suffix(1, 'и')
  .on(/ь$/).suffix(1, 'и')

const FemininePluralRuleSet = RuleSet.make()
  .on(/[яь]$/).suffix(1, 'и')
  .on(/[гкхжчшщ]а$/).suffix(1, 'и')
  .on(/а$/).suffix(1, 'ы')

const NeuterPluralRuleSet = RuleSet.make()
  .on(/о$/).suffix(1, 'а')
  .on(/е$/).suffix(1, 'я')

export {
  RuleSet,
  NounFormGuess
}
