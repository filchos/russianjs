const CombiningAcuteAccent = '\u0301'

const InvalidAccentError = new Error('InvalidAccentError')
const SurplusAccentError = new Error('SurplusAccentError')

const vowelRegex = /[АЕИОУЫЭЮЯЁаеиоуыэюяё]/g
const invalidAccentRegex = /'[^АЕИОУЫЭЮЯЁаеиоуыэюяё]/
const joRegex = /[Ёё]/g

class Term {
  constructor (code, str) {
    this.code = code
    this.content = str
  }

  get atomic () {
    return true
  }
}

class TermList {
  constructor (code) {
    this.code = code
    this.content = []
  }

  add (str) {
    this.content.push(str)
  }

  join (separator) {
    return this.content.join(separator)
  }

  get length () {
    return this.content.length
  }

  toJSON () {
    return { content: this.content }
  }
}

class RuTerm extends Term {
  constructor (raw, stressIndex) {
    super('ru', raw)
    this.stressIndex = stressIndex
    this.ext = {}
  }

  get accented () {
    const ix = this.stressIndex
    if (ix === -1) {
      return this.content
    } else {
      return this.content.substring(0, ix + 1) + CombiningAcuteAccent + this.content.substring(ix + 1)
    }
  }

  toJSON () {
    return {
      content: this.content,
      accented: this.accented
    }
  }
}

class RuPhrase {
  constructor () {
    this.code = 'ru'
    this.terms = []
    this.ext = {}
  }

  add (term) {
    this.terms.push(term)
  }

  get content () {
    return this.terms.map(term => term.content).join(' ')
  }

  get accented () {
    return this.terms.map(term => term.accented).join(' ')
  }

  get atomic () {
    return false
  }

  toJSON () {
    return {
      content: this.content,
      accented: this.accented
    }
  }
}

RuTerm.parse = (raw) => {
  if (raw.includes(' ')) {
    const phrase = new RuPhrase()
    for (raw of raw.split(' ')) {
      phrase.add(RuTerm.parse(raw))
    }
    return phrase
  }
  if (raw.endsWith("'") || raw.match(invalidAccentRegex)) {
    throw InvalidAccentError
  }
  const ca = raw.split("'").length - 1
  const cv = (raw.match(vowelRegex) || []).length
  const cj = (raw.match(joRegex) || []).length
  if (cv === 1 && ca > 0) {
    throw SurplusAccentError
  }
  if ((ca + cj) > 1) {
    throw SurplusAccentError
  }
  const index = raw.indexOf("'")
  raw = raw.replace(/'/, '')
  return new RuTerm(raw, index)
}

export {
  CombiningAcuteAccent,
  InvalidAccentError,
  RuTerm,
  SurplusAccentError,
  Term,
  TermList
}
