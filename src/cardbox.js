/* global localStorage */

class Collection {
  constructor (driver) {
    this.driver = driver
    this.boxes = {}
  }

  add (box) {
    box.driver = this.driver
    this.boxes[box.name] = box
  }

  get keys () {
    let keys = new Set()
    for (const box of Object.values(this.boxes)) {
      keys = new Set([...keys, ...box.keys])
    }
    return keys
  }

  get length () {
    let l = 0
    for (const box of Object.values(this.boxes)) {
      l += box.length
    }
    return l
  }

  get weightedLength () {
    let l = 0
    for (const box of Object.values(this.boxes)) {
      l += box.weightedLength
    }
    return l
  }

  pickRandom (dict) {
    if (dict.length === 0) {
      return null
    }

    // brute force method.
    // TODO: find smarter set to interset boxes and dict without destroying
    //       boxes on save.
    for (let c = 0; c < 10000; ++c) {
      const index = Math.floor(Math.random() * this.weightedLength)
      const result = this.pickByWeightedIndex(index)
      if (dict.has(result)) {
        return result
      }
    }
    return null
  }

  pickByWeightedIndex (index) {
    for (const box of Object.values(this.boxes)) {
      const bLen = box.keys.size * box.weight
      if (index < bLen) {
        const keys = [...box.keys]
        const key = keys[Math.floor(index / box.weight)]
        return { boxName: box.name, key }
      } else {
        index -= bLen
      }
    }
    return null
  }

  load () {
    for (const box of Object.values(this.boxes)) {
      box.load()
    }
  }
}

class Box {
  constructor (name, weight = 1) {
    this.name = name
    this.weight = weight
    this.rules = {}
    this.keys = new Set()
    this.driver = null
  }

  add (key) {
    this.keys.add(key)
  }

  has (key) {
    return this.keys.has(key)
  }

  remove (key) {
    this.keys.delete(key)
  }

  addRule (action, box) {
    this.rules[action] = box
  }

  equals (other) {
    return this.name === other.name
  }

  next (action, key) {
    const targetBox = this.rules[action]
    if (targetBox) {
      if (this.equals(targetBox)) {
        return
      }
      targetBox.add(key)
      targetBox.save()
      this.remove(key)
      this.save()
    }
  }

  load () {
    if (this.driver) {
      this.keys = this.driver.loadBoxKeys(this)
    }
  }

  save () {
    if (this.driver) {
      this.driver.saveBoxKeys(this)
    }
  }

  get length () {
    return this.keys.size
  }

  get weightedLength () {
    return this.keys.size * this.weight
  }
}

class LocalStorageDriver {
  constructor (prefix) {
    this.prefix = prefix
  }

  loadBoxKeys (box) {
    const raw = localStorage.getItem(this.prefix + box.name)
    if (raw) {
      return new Set(JSON.parse(raw))
    } else {
      return new Set()
    }
  }

  saveBoxKeys (box) {
    const raw = JSON.stringify(Array.from(box.keys))
    localStorage.setItem(this.prefix + box.name, raw)
  }
}

export { Box, Collection, LocalStorageDriver }
