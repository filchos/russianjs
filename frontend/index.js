/* global Audio */

import { addObserver, addRoute, runDispatcher } from './router.js'

addRoute(/^\/$/, './handler/indexHandler')
addRoute(/^\/word\/(.+)$/, './handler/wordHandler')
addRoute(/^\/quiz$/, './handler/quizHandler')
addObserver('click', 'a:not([rel=external]', elt => elt.href)
runDispatcher()

document.addEventListener('keydown', (e) => {
  const elt = document.querySelector('*[data-click-key-code="' + e.keyCode + '"]')
  if (elt && elt.offsetWidth && elt.offsetHeight) {
    elt.click()
  }
})

document.addEventListener('click', (e) => {
  const audioURL = e.target.dataset.audio
  if (audioURL) {
    (new Audio(audioURL)).play()
  }
})

console.log('Inited app')
