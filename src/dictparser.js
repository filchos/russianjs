import { LineParser, ParseError } from './parser.js'
import { posAbbrs, propAbbrs } from './abbrs.js'
import props from './props.js'
import { RuTerm } from './term.js'
import { Adjective, DemonstrativePronoun, Name, PersonalPronoun, Phrase, PossessivePronoun, Noun, Verb } from './vocable.js'

const MsgInconvenientProp = 'inconvenient property'
const MsgMissingProp = 'missing property'
const MsgSurplusProp = 'surplus property'
const MsgUnknownPropAbbr = 'unknown property abbreviation'

// extra values handled as arrays

const extraArrayProps = ['level', 'see']

// properties and abbreviations

const propMap = {
  [props.Animate]: ['animacy', Noun],
  [props.Inanimate]: ['animacy', Noun],

  [props.Imperfective]: ['aspect', Verb],
  [props.Perfective]: ['aspect', Verb],

  [props.Accusative]: ['case', Adjective, DemonstrativePronoun, Name, Noun, PersonalPronoun],
  [props.Dative]: ['case', Adjective, DemonstrativePronoun, Name, Noun, PersonalPronoun],
  [props.Genitive]: ['case', Adjective, DemonstrativePronoun, Name, Noun, PersonalPronoun],
  [props.Instrumental]: ['case', Adjective, DemonstrativePronoun, Name, Noun, PersonalPronoun],
  [props.Nominative]: ['case', Adjective, DemonstrativePronoun, Name, Noun, PersonalPronoun],
  [props.Prepositional]: ['case', Adjective, DemonstrativePronoun, Name, Noun, PersonalPronoun],

  [props.Feminine]: ['gender', Adjective, DemonstrativePronoun, Name, Noun, PossessivePronoun],
  [props.Masculine]: ['gender', Adjective, DemonstrativePronoun, Name, Noun, PossessivePronoun],
  [props.Neuter]: ['gender', Adjective, DemonstrativePronoun, Name, Noun, PossessivePronoun],

  [props.Honorific]: ['honorific', PersonalPronoun, PossessivePronoun, Verb],

  [props.Subjunctive]: ['mood', Verb],
  [props.Imperative]: ['mood', Verb],
  [props.Indicative]: ['mood', Verb],

  [props.Singular]: ['number', Noun],
  [props.Plural]: ['number', Adjective, DemonstrativePronoun, Noun, PossessivePronoun],

  [props.Singular1st]: ['person', PersonalPronoun, PossessivePronoun, Verb],
  [props.Singular2nd]: ['person', PersonalPronoun, PossessivePronoun, Verb],
  [props.Singular3rd]: ['person', Verb],
  [props.Singular3rdM]: ['person', PersonalPronoun, PossessivePronoun],
  [props.Singular3rdF]: ['person', PersonalPronoun, PossessivePronoun],
  [props.Singular3rdN]: ['person', PersonalPronoun, PossessivePronoun],
  [props.Plural1st]: ['person', PersonalPronoun, PossessivePronoun, Verb],
  [props.Plural2nd]: ['person', PersonalPronoun, PossessivePronoun, Verb],
  [props.Plural3rd]: ['person', PersonalPronoun, PossessivePronoun, Verb],

  [props.Future]: ['tense', Verb],

  [props.Finite]: ['vform', Verb],
  [props.Infinitive]: ['vform', Verb]
}

// handlers

function formHandler (ctx, line, childStr, parentStr, propAndExtraStr) {
  // we postpone the actual handling to the end, when all words are in the dict
  ctx.formDefers.push([line, childStr, parentStr, propAndExtraStr])
}

function formDeferHandler (ctx, line, childStr, parentStr, propAndExtraStr) {
  parentStr = parentStr.trim()
  childStr = childStr.trim()

  const stub = { key: RuTerm.parse(parentStr).accented }

  if (!ctx.dict.has(stub)) {
    throw new Error('parent for ' + stub.key + ' not found')// TODO better error
  }
  const parent = ctx.dict.get(stub)
  const Factory = parent.constructor

  let voc = new Factory(RuTerm.parse(childStr))

  // add props and extras
  voc.inheritPropsFromParent(parent)
  voc.extra = { ...ctx.extra }
  voc = parsePropsAndExtras(line, voc, propAndExtraStr)

  parent.forms.push(voc)
}

function addLevelHandler (ctx, line, ru) {
  const stub = { key: RuTerm.parse(ru).accented }
  if (!ctx.dict.has(stub)) {
    throw new Error('vocable for ' + stub.key + ' not found in ' + line)
  }
  const voc = ctx.dict.get(stub)
  voc.extra.level = [...voc.extra.level, ...ctx.extra.level]
}

function exampleHandler (ctx, line, ru, de, rawkeys) {
  // we postpone the actual handling to the end, when all words are in the dict
  ctx.exampleDefers.push([line, ru, de, rawkeys])
}

function exampleDeferHandler (ctx, line, ru, de, rawkeys) {
  for (const rawKey of rawkeys.split(' ')) {
    const stub = { key: RuTerm.parse(rawKey).accented }
    if (!ctx.dict.has(stub)) {
      throw new Error('vocable for ' + stub.key + ' not found in ' + line)
    }
    const example = new Phrase(RuTerm.parse(ru))
    example.addTranslation('de', de)
    ctx.dict.get(stub).examples.push(example)
  }
}

function wordHandler (ctx, line, ruStr, deStr, pos, propAndExtraStr) {
  ruStr = ruStr.trim()
  deStr = deStr.trim()

  // create voc based on word type
  const Factory = posAbbrs[pos]
  if (!Factory) {
    throw new Error('No factory for ' + pos + ' found')
  }
  let voc = new Factory(RuTerm.parse(ruStr))

  // add translation(s)
  for (const dePiece of deStr.split(',')) {
    voc.addTranslation('de', dePiece.trim())
  }

  // add props and extras
  voc.extra = { ...ctx.extra }
  voc = parsePropsAndExtras(line, voc, propAndExtraStr)

  // add voc to dict
  ctx.dict.add(voc)
}

function keyValuesHandler (ctx, line) {
  for (const extra of line.split(',')) {
    const { k, v } = splitExtra(extra)
    if (v === '') {
      delete ctx.extra[k]
    } else if (extraArrayProps.includes(k)) {
      ctx.extra[k] = [v]
    } else {
      ctx.extra[k] = v
    }
  }
}

// helpers

function splitExtra (extra) {
  const ix = extra.indexOf(':')
  return {
    k: extra.substring(0, ix).trim(),
    v: extra.substring(ix + 1).trim()
  }
}

function parsePropsAndExtras (line, voc, propAndExtraStr) {
  propAndExtraStr = (propAndExtraStr || '').trim()
  let propsAndExtras = []
  if (propAndExtraStr) {
    propsAndExtras = propAndExtraStr.split(' ')
  }
  const propInput = propsAndExtras.filter(item => !item.includes(':'))
  const extraInput = propsAndExtras.filter(item => item.includes(':'))

  // handle props
  const props = {}
  for (const propAbbr of propInput) {
    const prop = propAbbrs[propAbbr]
    if (!prop) {
      throw new ParseError(line, MsgUnknownPropAbbr + ': ' + propAbbr)
    }

    const propConfig = propMap[prop]
    if (!propConfig) {
      throw new Error('No prop config for ' + prop)
    }

    const [group, ...poses] = propConfig
    if (!poses.includes(voc.constructor)) {
      throw new ParseError(line, MsgInconvenientProp + ': ' + prop)
    }

    if (props[group]) {
      throw new ParseError(line, MsgSurplusProp + ': ' + prop)
    }
    props[group] = prop
  }
  voc.props = { ...voc.props, ...props }

  // handle extras
  for (const extra of extraInput) {
    const { k, v } = splitExtra(extra)
    if (extraArrayProps.includes(k)) {
      voc.extra[k] = [v]
    } else {
      voc.extra[k] = v
    }
  }

  return voc
}

// class

const FORM_REGEX = /^-\s*(\S*)\s\s+(\S*)\s\s+(.+)$/
const WORD_REGEX = /^(.+)\s\s+(\S.*)\s\s+(Adj|Adv|Cj|Dp|Ip|N|Name|Num|Pers|Ph|Poss|Prep|V|Voc)(?:\s\s+(\S.*))?$/
const KV_REGEX = /^(.+:.*)(?:,(.+:.*))*$/
const EXAMPLE_REGEX = /^"(.+)"\s*"(.+)"\s*(.+)$/
const ADD_LEVEL_REGEX = /^(.*\S)\s*\+\+$/

class RuDictLineParser extends LineParser {
  constructor () {
    super()
    this.ctx.extra = {}
    this.ctx.formDefers = []
    this.ctx.exampleDefers = []
    this.register(FORM_REGEX, formHandler)
    this.register(WORD_REGEX, wordHandler)
    this.register(KV_REGEX, keyValuesHandler)
    this.register(EXAMPLE_REGEX, exampleHandler)
    this.register(ADD_LEVEL_REGEX, addLevelHandler)
    this.register(/^$/, line => null)
  }

  prepare (line) {
    return line.replace(/\s*#.*$/, '').trim()
  }

  finalize () {
    const dict = this.ctx.dict

    // handle deferred
    for (const args of this.ctx.formDefers) {
      formDeferHandler(this.ctx, ...args)
    }
    for (const args of this.ctx.exampleDefers) {
      exampleDeferHandler(this.ctx, ...args)
    }

    // backrefs for see
    for (const [key, vocable] of Object.entries(dict.vocables)) {
      const see = vocable.extra.see
      if (see) {
        const stub = { key: see }
        // check,if the related key exists
        if (dict.has(stub)) {
          // add back reference
          const ref = dict.get(stub)
          if (ref.extra.see) {
            ref.extra.see.push(key)
          } else {
            ref.extra.see = [key]
          }
        }
      }
    }

    dict.sortByKey()
    return dict
  }
}

export {
  MsgInconvenientProp,
  MsgMissingProp,
  MsgSurplusProp,
  MsgUnknownPropAbbr,
  RuDictLineParser
}
