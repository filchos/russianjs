/* global location */

import { posNameAbbrs } from '../../src/abbrs.js'
import props from '../../src/props.js'
import { RuTerm } from '../../src/term.js'

// TODO: get from all existing levels:

const levels = {
  A1: 'A1',
  A2: 'A2',
  'Pons 1': 'Pons 1',
  'Pons 5': 'Pons 5',
  Aliia: 'Aliia'
}

const renderFilters = (action, filters) => {
  return `
    <form id="filters" method="get" action="${action}">
      <fieldset>
        <label for="pos">Part of speech:</label>
        ${renderFilter('pos', filters.pos, posNameAbbrs)}
        <label for="level">Tag:</label>
        ${renderFilter('level', filters.level, levels)}
      </fieldset>
      <a href="${location.pathname}" class="reset">⦻</a>
    </form>
  `
}

const renderList = (dict) => {
  if (dict.length === 0) {
    return '<p class="msg"><em>Empty list</em></p>'
  }
  return `
    <table class="vocable-list">
      <thead>
        <tr>
          <th>Russian</th>
          <th class="no-print"></th>
          <th>German</th>
          <th class="no-print">Part of speech</th>
          <th>Properties</th>
          <th class="no-print">Level</th>
          <th class="no-print">Forms</th>
        </tr>
      </thead>
      <tbody>
        ${Object.values(dict.vocables).map(renderRow).join('')}
      </tbody>
    </table>
  `
}

const renderPersonalPronounTable = (dict) => {
  const row = {
    [props.Singular1st]: null,
    [props.Singular2nd]: null,
    [props.Singular3rdM]: null,
    [props.Singular3rdF]: null,
    [props.Singular3rdN]: null,
    [props.Plural1st]: null,
    [props.Plural2nd]: null,
    [props.Plural3rd]: null,
    honorific: null
  }
  const forms = {
    [props.Nominative]: { ...row },
    [props.Accusative]: { ...row },
    [props.Dative]: { ...row },
    [props.Genitive]: { ...row },
    [props.Instrumental]: { ...row },
    [props.Prepositional]: { ...row }
  }

  for (const voc of Object.values(dict.vocables)) {
    const person = voc.props.honorific ? 'honorific' : voc.props.person
    forms[voc.props.case][person] = voc.ru
    for (const sub of voc.forms) {
      forms[sub.props.case][person] = sub.ru
    }
  }

  let content = ''
  for (const [person, row] of Object.entries(forms)) {
    content += '<tr><th><em>' + person + '</em></th>'
    for (const term of Object.values(row)) {
      content += '<td lang="ru">' + (term ? renderRu(term) : '') + '</td>'
    }
    content += '</tr>'
  }

  return `
  <table class="vocable-list">
    <thead>
      <tr>
        <th rowspan="3">Case</th>
        <th colspan="5">Singular</th>
        <th colspan="3">Plural</th>
        <th rowspan="3">Honorific</th>
      </tr>
      <tr>
        <th rowspan="2">1</th>
        <th rowspan="2">2</th>
        <th colspan="3">3</th>
        <th rowspan="2">1</th>
        <th rowspan="2">2</th>
        <th rowspan="2">3</th>
      </tr>
      <tr>
        <th>m</th>
        <th>f</th>
        <th>n</th>
      </tr>
    </thead>
    <tbody>${content}</tbody>
  </table>
  `
}

const renderNav = (total, numberOf) => {
  return `
    <nav id="main">
      <ul>
        <li class="list-link"><a href="/${location.search}">Word list</a><span>${numberOf}</span></li>
        <li class="quiz-link"><a href="/quiz${location.search}">Quiz</a><span>${numberOf}</span></li>
      </ul>
    </nav>
  `
}

const renderQuizControls = () => {
  return `
    <ul class="quiz-controls">
      <li class="reveal"><span data-click-key-code="32" class="action-reveal">Reveal</span></li>
      <li class="next"><span data-click-key-code="38" class="action-win">Yes ⬆︎</span></li>
      <li class="next"><span data-click-key-code="40" class="action-loose">No ⬇︎</span></li>
    </ul>
    `
}

const renderQuizStats = (collection) => {
  const num = Object.values(collection.boxes).filter(box => box.length > 0).length
  const l = collection.length
  const min = 9
  const pct = (ll) => (min + ((100 - num * min) * ll / l)) + '%'
  const bars = []
  for (const [name, box] of Object.entries(collection.boxes)) {
    if (box.length > 0) {
      bars.push(`
        <li class="${name}-box" style="width:${pct(box.length)}">
          <a href="${location.pathname}?box=${name}">
            <span>${name.toUpperCase()}</span>
            ${box.length}
          </a>
        </li>
      `)
    }
  }

  return `<ul class="stats">${bars.join('')}</ul>`
}

const renderVoc = (voc) => {
  return `
    <div class="vocable-card vocable type-${voc.pos.toLowerCase()}">
      <h3>${voc.pos}</h3>
      <h1 lang="ru">${renderRu(voc.ru)}<i>(${voc.ru.content})</i></h1>
      <h2 lang="de">${voc.translation('de')}</h2>
      ${renderProps(voc.props)}
      ${renderExtra(voc.extra)}
      ${renderExternals(voc.ru.ext)}
      ${renderForms(voc)}
      ${renderExamples(voc)}
    </div>
    `
}

/// /////////

const renderExamples = (voc) => {
  if (voc.examples.length === 0) {
    return ''
  }
  return `
    <table class="examples">
      <caption>Beispiel${voc.examples.length > 1 ? 'e' : ''}</caption>
      ${voc.examples.map(renderExample).join('')}
    </table>
  `
}

const renderExample = (example) => {
  return `
    <tr>
      <td lang="ru">${renderRu(example.ru)}</td>
      <td lang="de">${example.translation('de')}</td>
    </tr>
  `
}

const renderExternals = (ext) => {
  const pieces = []
  if (ext.wiktionaryURL) {
    pieces.push(`<li class="wiktionary"><a href="${ext.wiktionaryURL}" rel="external" target="_blank"></a></li>`)
  }
  if (ext.wikimediaAudioURLs) {
    for (const url of ext.wikimediaAudioURLs) {
      pieces.push(`<li class="audio"><span data-audio="${url}"></span></li>`)
    }
  }
  if (pieces.length) {
    return `<ul class="ext">${pieces.join('')}</ul>`
  } else {
    return ''
  }
  // return '<code>' + JSON.stringify(ext) + '</code>'
}

const renderFilter = (name, value, map) => {
  return `
    <select id="${name}" name="${name}">
      <option></option>
      ${Object.entries(map).map(pair => renderFilterItem(pair[0], pair[1], pair[0] === value)).join('')}
    </select>
  `
}

const renderFilterItem = (k, v, active) => {
  return `<option value="${k}"${active ? ' selected="selected"' : ''}>${v}</option>`
}

const renderRow = (voc) => {
  return `
    <tr data-key="${voc.key}">
      <th lang="ru">${renderRu(voc.ru)}</th>
      <td class="no-print" lang="ru"><em>${voc.ru.content}</em></td>
      <td lang="de">${voc.translation('de')}</td>
      <td class="no-print">${voc.pos}</td>
      <td>${renderPropLine(voc.props)}</td>
      <td class="no-print"><small>${voc.extra.level.join(', ')}</small></td>
      <td class="right no-print"><small>${voc.forms.length ? voc.forms.length : ''}</small></td>
    </tr>`
}

const renderRu = (ru) => {
  return ru.accented.replace(/(.\u0301|ё)/gi, '<b>$1</b>')
}

const renderExtra = (extra) => {
  extra = { ...extra }
  delete extra.cclass
  delete extra.level
  delete extra.link
  delete extra.origin
  delete extra.variant
  const pairs = Object.entries(extra)
  if (pairs.length === 0) {
    return ''
  }
  return `<ul class="label-list">${pairs.map(p => renderTLabel(`extra-${p[0]}`, ...p)).join('')}</ul>`
}

const renderForms = (voc) => {
  if (voc.forms.length === 0) {
    return ''
  }
  return `<ul class="forms">${voc.forms.map(f => renderForm(voc, f)).join('')}</ul>`
}

const renderForm = (parent, voc) => {
  const props = { ...voc.props }
  for (const [k, v] of Object.entries(parent.props)) {
    if (props[k] === v) {
      delete props[k] // remove prop, when it has the same value in parent voc
    }
  }
  return `
    <li>
      <div class="vocable-card vocable">
        <h1 lang="ru">${renderRu(voc.ru)}</h1>
        ${renderProps(props)}
      </div>
    </li>
`
}

const stopProps = [
  'animacy-inanimate',
  'case-nominative',
  'mood-indicative',
  'number-singular',
  'tense-present',
  'vform-finite',
  'vform-infinitive'
]

const renderPropLine = (props) => {
  const pairs = Object.entries(props)
    .filter(p => !!p[1])
    .filter(p => !stopProps.includes(p.join('-')))
  if (pairs.length === 0) {
    return ''
  }
  return pairs.map((p) => `
    <span class="prop propgroup-${p[0]} prop-${p[1]}">
      <span class="group">${p[0]}</span>
      <span class="value">${p[1]}</span>
    </span>
  `).join(' · ')
}

const renderProps = (props) => {
  const pairs = Object.entries(props).filter(p => !!p[1]).filter(p => p[1] !== 'finite' && p[1] !== 'indicative')
  if (pairs.length === 0) {
    return ''
  }
  return `<ul class="label-list">${pairs.map(p => renderTLabel(`propgroup-${p[0]} prop-${p[1]}`, ...p)).join('')}</ul>`
}

const renderTLabel = (classes, key, value) => {
  if (Array.isArray(value)) {
    return value.map(v => renderTLabel(classes, key, v)).join("")
  }
  if (key === 'see') {
    value = RuTerm.parse(value).accented
    value = `<a href="/word/${value}">${value}</a>`
  }
  return `<li class="${classes}"><span class="group"><span>${key}</span></span><span class="prop">${value}</span></li>`
}

export {
  renderFilters,
  renderList,
  renderNav,
  renderPersonalPronounTable,
  renderQuizControls,
  renderQuizStats,
  renderVoc
}
