/* global location */

import { Box, Collection, LocalStorageDriver } from '../../src/cardbox.js'
import { DictService, FilterService } from '../service.js'
import { renderFilters, renderNav, renderQuizControls, renderQuizStats, renderVoc } from '../renderer.js'
import { loadRoute } from '../router.js'

let boxes
let currentVoc = null // used by actions
let currentBox = null // used by actions

async function init () {
  const driver = new LocalStorageDriver('cardbox-')
  boxes = new Collection(driver)
  const box4 = new Box('good', 1)
  const box3 = new Box('well', 2)
  const box2 = new Box('new', 10)
  const box1 = new Box('learn', 5)
  const box0 = new Box('cellar', 8)
  box4.rules = { yes: box4, no: box1 }
  box3.rules = { yes: box4, no: box1 }
  box2.rules = { yes: box3, no: box0 }
  box1.rules = { yes: box3, no: box0 }
  box0.rules = { yes: box1, no: box0 }
  boxes.add(box4)
  boxes.add(box3)
  boxes.add(box2)
  boxes.add(box1)
  boxes.add(box0)
  boxes.load()

  const dict = await DictService.get()

  const existInBoxes = boxes.keys
  const missingInBoxes = new Set([...dict.keys].filter(x => !existInBoxes.has(x)))
  if (missingInBoxes.size > 0) {
    console.log('Add', missingInBoxes.size, 'new words')
    box2.keys = new Set([...box2.keys, ...missingInBoxes])
    box2.save()
  }
}
init()

export default async () => {
  let dict = await DictService.get()
  const total = dict.length
  dict = FilterService.byCurrentURL(dict)
  const filterParams = FilterService.getURLParams()

  if (boxes.length === 0 || dict.length === 0) {
    loadRoute('/quiz')
    return
  }

  const pick = boxes.pickRandom(dict)
  if (!pick) {
    throw new Error('pickRandom is buggy or boxes are empty')
  }
  currentVoc = dict.get({ key: pick.key })
  currentBox = boxes.boxes[pick.boxName]

  document.title = `Quiz „${currentVoc.translation('de')}“`
  const stage = document.getElementById('app')
  stage.className = 'quiz-mode hide'

  stage.innerHTML = `
    <header>
      ${renderNav(total, dict.length)}
      ${renderFilters('/quiz', filterParams)}
    </header>
    <main>
      ${renderVoc(currentVoc)}
    </main>
    <footer>
      ${renderQuizControls()}
      ${renderQuizStats(boxes)}
    </footer>`

  stage.querySelector('.action-reveal').addEventListener('click', reveilAction)
  stage.querySelector('.action-win').addEventListener('click', winAction)
  stage.querySelector('.action-loose').addEventListener('click', looseAction)

  stage.querySelector('select#pos').addEventListener('change', (e) => e.target.closest('form').submit())
  stage.querySelector('select#level').addEventListener('change', (e) => e.target.closest('form').submit())
}

const reveilAction = (e) => {
  document.querySelector('#app').classList.remove('hide')
  document.title += ` – «${currentVoc.ru.content}»`
}

const winAction = (e) => {
  currentBox.next('yes', currentVoc.key)
  loadRoute('/quiz' + location.search)
}

const looseAction = (e) => {
  currentBox.next('no', currentVoc.key)
  loadRoute('/quiz' + location.search)
}
