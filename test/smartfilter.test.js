/* globals expect test */

import smartFilter from '../src/smartfilter.js'

test('smart filter', () => {
  const tests = [
    // EQUALS
    ['a term', 'a term', true],
    ['a term', 'term', false],
    // OR
    ['a term', 'a term, another term', true],
    ['another term', 'a term, another term', true],
    ['a third term', 'a term, another term', false],
    // NOT
    ['a term', '!a term', false],
    ['a term', '!another term', true],
    // NOT AND
    ['a term', '!a term,!another term', false],
    // ENDS WITH
    ['a term', '*term', true],
    ['a term', '*ter', false],
    // STARTS WITH
    ['a term', 'a*', true],
    ['a term', 'term*', false],
    // CONTAINS
    ['a longer term', '*long*', true],
    ['a longer term', '*a l*', true],
    ['a longer term', '*short*', false],
    // BEFORE
    ['2020-02-01', '..2020-02-02', true],
    ['2020-02-02', '..2020-02-02', true],
    ['2020-02-03', '..2020-02-02', false],
    // AFTER
    ['2020-02-01', '2020-02-02..', false],
    ['2020-02-02', '2020-02-02..', true],
    ['2020-02-03', '2020-02-02..', true],
    // BETWEEN
    ['2020-02-01', '2020-02-02..2021-12-02', false],
    ['2020-02-02', '2020-02-02..2021-12-02', true],
    ['2020-08-08', '2020-02-02..2021-12-02', true],
    ['2021-12-02', '2020-02-02..2021-12-02', true],
    ['2021-12-03', '2020-02-02..2021-12-02', false],
    // REGEXP
    ['word', '/(word|phrase)/', true],
    ['Wort', '/(word|phrase)/', false],
    // COMBINATION
    ['this is a term', 'this*,*is*,*a term,!short,tg..,..tj', true]
  ]

  for (const [str, pattern, expected] of tests) {
    expect(smartFilter(pattern).matches(str)).toBe(expected)
  }
})

test('smart filter failing pattern', () => {
  const t = () => {
    smartFilter('..invalid..')
  }
  expect(t).toThrow(Error)
})
