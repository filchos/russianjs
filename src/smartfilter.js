class SmartFilter {
  constructor (pattern, valueMap = null) {
    this.patterns = pattern.split(',').map(pattern => pattern.trim())
    this.valueMap = valueMap
    this.allTesters = []
    this.anyTesters = []
    for (pattern of this.patterns) {
      const tester = this.getTester(pattern)
      if (tester.all) {
        this.allTesters.push(tester)
      } else {
        this.anyTesters.push(tester)
      }
    }
  }

  getTester (pattern) {
    // special case: between
    const m = pattern.match(/^(.+)\.\.(.+)$/)
    if (m) {
      return (str) => str >= this.prepareString(m[1]) && str <= this.prepareString(m[2])
    }

    let [prefix, term, postfix] = pattern.match(/^(!|\.\.|\*|\/)?(.+?)(\.\.|\*|\/)?$/).slice(1)
    term = this.prepareString(term)

    const pat = (prefix ?? '') + ' ' + (postfix ?? '')

    switch (pat) {
      case ' ':
        return (str) => str === term
      case '! ':
      {
        const fn = (str) => str !== term
        fn.all = true
        return fn
      }
      case '* ':
        return (str) => str.endsWith(term)
      case ' *':
        return (str) => str.startsWith(term)
      case '* *':
        return (str) => str.includes(term)
      case '.. ':
        return (str) => str <= term
      case ' ..':
        return (str) => str >= term
      case '/ /':
        return (str) => (new RegExp(term)).test(str)
      default:
        throw new Error('Invalid smart filter pattern: ' + pat)
    }
  }

  prepareString (str) {
    if (this.valueMap) {
      return this.valueMap[str] ?? str
    } else {
      return str
    }
  }

  matches (str) {
    // pass 1: each allTesters must resolve to true.
    for (const tester of this.allTesters) {
      if (!tester(str)) {
        return false
      }
    }
    // if there are no anyTesters left, return true
    if (this.anyTesters.length === 0) {
      return true
    }
    // pass 2: at least one of anyTesters must resolve to true.
    for (const tester of this.anyTesters) {
      if (tester(str)) {
        return true
      }
    }
    return false
  }
}

export default (pattern, valueMap = null) => {
  return new SmartFilter(pattern, valueMap)
}
