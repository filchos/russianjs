import { DictService, FilterService } from '../service.js'
import { renderFilters, renderNav, renderVoc } from '../renderer.js'

export default async (all, encodedKey) => {
  let dict = await DictService.get()
  const total = dict.length
  dict = FilterService.byCurrentURL(dict)
  const filterParams = FilterService.getURLParams()

  const key = decodeURI(encodedKey)
  const voc = dict.get({ key })

  document.title = `«${voc.ru.content}» – „${voc.translation('de')}“`
  const stage = document.getElementById('app')
  stage.className = 'word-mode'

  stage.innerHTML = `
    <header>
      ${renderNav(total, dict.length)}
      ${renderFilters('/', filterParams)}
    </header>
    <main>
      ${renderVoc(voc)}
    </main>`

  stage.querySelector('select#pos').addEventListener('change', (e) => e.target.closest('form').submit())
  stage.querySelector('select#level').addEventListener('change', (e) => e.target.closest('form').submit())
}
