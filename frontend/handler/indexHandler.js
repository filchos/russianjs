/* global location */

import { DictService, FilterService } from '../service.js'
import { renderFilters, renderList, renderNav, renderPersonalPronounTable } from '../renderer.js'
import { loadRoute } from '../router.js'

export default async () => {
  let dict = await DictService.get()
  const total = dict.length
  dict = FilterService.byCurrentURL(dict)
  const filterParams = FilterService.getURLParams()

  document.title = 'Home'
  const stage = document.getElementById('app')
  stage.className = 'list-mode'

  let content
  if (filterParams.pos === 'Pers') {
    content = renderPersonalPronounTable(dict)
  } else {
    content = renderList(dict)
  }

  stage.innerHTML = `
    <header>
      ${renderNav(total, dict.length)}
      ${renderFilters('/', filterParams)}
    </header>
    <main>
      ${content}
    </main>`

  const tbody = stage.querySelector('table.vocable-list tbody')
  if (tbody) {
    tbody.addEventListener('click', gotoAction)
  }
  stage.querySelector('select#pos').addEventListener('change', (e) => e.target.closest('form').submit())
  stage.querySelector('select#level').addEventListener('change', (e) => e.target.closest('form').submit())
}

const gotoAction = (e) => {
  const key = e.target.closest('tr').dataset.key
  loadRoute('/word/' + encodeURI(key) + location.search)
}
