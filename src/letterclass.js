const classes = {
  Consonants: 'БбВвГгДдЖжЗзЙйКкЛлМмНнПпРрСсТтФфХхЦцЧчШшЩщ'.split(''),
  HardVowels: 'АаОоУуЫыЭэ'.split(''),
  Signs: 'ЪъЬь'.split(''),
  SoftVowels: 'ЕеЁёИиЮюЯя'.split(''),
  Vowels: 'АаЕеЁёИиОоУуЫыЭэЮюЯя'.split('')
}

function isConsonant (letter) {
  return classes.Consonants.includes(letter)
}

function isHardVowel (letter) {
  return classes.HardVowels.includes(letter)
}

function isSign (letter) {
  return classes.Signs.includes(letter)
}

function isSoftVowel (letter) {
  return classes.SoftVowels.includes(letter)
}

function isVowel (letter) {
  return classes.Vowels.includes(letter)
}

export {
  isConsonant,
  isHardVowel,
  isSign,
  isSoftVowel,
  isVowel
}
