import fs from 'fs'
import readline from 'readline'

import { RuDictLineParser } from './dictparser.js'

const parseRuDictFile = async (dictPath) => {
  const rdp = new RuDictLineParser()
  const lineReader = readline.createInterface({
    input: fs.createReadStream(dictPath)
  })

  return new Promise(
    (resolve, reject) => {
      lineReader.on('line', parseLine)
      lineReader.on('close', finalize)

      function parseLine (line) {
        try {
          rdp.dispatch(line)
        } catch (e) {
          reject(e)
        }
      }

      function finalize () {
        return resolve(rdp.finalize())
      }
    }
  )
}

export default parseRuDictFile
