/* global location, URL */

/**
 * a lightweight router and dispatcher for the frontend using the History api
 * @module router
 */

/**
 * a list of objects to map a regex pattern to a handler name.
 * @type { Array<{pattern: RegExp, name: string}> }
 */
const routes = []

/**
 * the absolute path of the javascript root directory. Is used to replace @app in the handler
 * strings by this path.
 * @type { string }
 */
let jsRootPath = ''

/**
 * sets the absolute path of the javascript root directory
 * @function
 * @param { string } absDir the absolute path of the javascript root directory
 */
export const setJsRootDir = (absDir) => {
  jsRootPath = absDir
}

/**
 * sets the absolute path of the javascript root directory by the module info of the index.js
 * @function
 * @param { Object } module info
 */
export const setJsRootDirByMeta = (meta) => {
  const selfUrl = new URL(meta.url)
  const absFilePath = selfUrl.pathname
  const absDir = absFilePath.replace(/[^/]+$/, '')
  setJsRootDir(absDir)
}

/**
 * Add a route definition to the list.
 * @function
 * @param { RegExp } pattern the pattern that the url pathname has to match
 * @param { path }   path of the handler to load and to execute
 */
export const addRoute = (pattern, path) => {
  routes.push({ pattern, path })
}

/**
 * Add an observer that will load a new route
 * @function
 * @param { string }   eventType the event type as e.g. "click" or "change"
 * @param { string }   selector the query selector
 * @param { Callback } pathAccessor a callback function returning the desired path of the new route based on event
 */
export const addObserver = (eventType, selector, pathAccessor) => {
  document.addEventListener(
    eventType,
    (e) => {
      const target = e.target
      const elt = target.closest(selector)

      if (elt) {
        e.preventDefault()
        e.stopPropagation()
        const url = pathAccessor(elt)
        loadRoute(url)
      }
    }
    , false
  )
}

/**
 * The internal dispatcher.
 *
 * For each registered route the dispatcher tests the pattern against the url string
 * It loads the matching module with an async import and executes the function that is exported
 * with default.
 *
 * In addition to that a npdc-route-change event is dispatched. It has a detail containing the url
 * as an URL object
 *
 * The handler function is responsible for all further steps.
 * The first argument for the handler function is the url pathname, the other arguments are the
 * matched groups in the regex. The handler does not provide a return value.
 *
 * It no matching route was found, an Error is thrown.
 *
 * @function
 * @param { string } urlString the complete url as a string
 *
 * @todo custom error
 */
const dispatch = async (urlString) => {
  const url = new URL(urlString)
  const pathname = url.pathname
  let result
  for (const route of routes) {
    result = route.pattern.exec(pathname)
    if (result) {
      const fragments = Array.from(result)
      fragments[0] = pathname
      await executeHandler(route.path, ...fragments)

      const routeChangeEvent = new window.CustomEvent('npdc-route-change', { detail: { url } })
      document.dispatchEvent(routeChangeEvent)
      return
    }
  }
  throw new Error('No matching route')
}

/**
 * loads a module by path and calls the handler function. The handler has to export its function
 * with `export default`
 * the prefix @app in the path is replaced by the jsRootPath
 *
 * @function
 * @param { string }    the path of the module to load
 * @param { ...string } arguments for the handler function
 */
export const executeHandler = async (path, ...args) => {
  const absPath = path.replace(/^@app\//, jsRootPath) + '.js'
  const handlerModule = await import(absPath)
  const handler = handlerModule.default
  handler(...args)
}

/**
 * an internal shortcut for dispatch by the current location.pathname
 * @function
 */
const dispatchByLocation = () => {
  dispatch(window.location.href)
}

/**
 * loads a new route
 * @function
 * @param { string } url the url for the new route. Either a complete url with scheme and host, or a path starting with /
 */
export const loadRoute = (url) => {
  if (url.startsWith('/')) {
    url = location.origin + url
  }
  window.history.pushState({}, '', url)
  dispatch(url)
}

/**
 * run the dispatcher
 * Should be called after registering all routes and observers
 * @function
 */
export const runDispatcher = () => {
  window.addEventListener('DOMContentLoaded', dispatchByLocation)
  window.addEventListener('popstate', dispatchByLocation, false)
}
