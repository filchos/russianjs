/* globals expect test */

import 'regenerator-runtime/runtime'

import { MsgInconvenientProp, MsgSurplusProp, MsgUnknownPropAbbr, RuDictLineParser } from '../src/dictparser.js'
import { MsgNoMatch } from '../src/parser.js'
import props from '../src/props.js'
import { CombiningAcuteAccent } from '../src/term.js'
import { PersonalPronoun } from '../src/vocable.js'

test('true', () => {
  expect(true).toBeTruthy()
})

test('prepare', () => {
  const rdp = new RuDictLineParser()

  expect(rdp.prepare('line')).toBe('line')
  expect(rdp.prepare('  line ')).toBe('line')
  expect(rdp.prepare('line# comment ')).toBe('line')
  expect(rdp.prepare('line  #comment')).toBe('line')
})

test('wordhandler exceptions', () => {
  const rdp = new RuDictLineParser()

  const map = {
    // cannot not be parsed
    ххх: MsgNoMatch,
    // Unknown word class. Cannot not be parsed
    'ххх  XXX  X  xxx': MsgNoMatch,
    // Unknown prop (not in abbrs)
    'ххх  XXX  Voc  xxx': MsgUnknownPropAbbr,
    // Prop that is not conveniant for the word class
    'ххх  XXX  N  imp!': MsgInconvenientProp,
    // Two props of the same property (here: gender)
    'ххх  XXX  N  m f': MsgSurplusProp
  }

  expect.assertions(Object.keys(map).length)

  for (const [line, msg] of Object.entries(map)) {
    try {
      rdp.dispatch(line)
    } catch (e) {
      expect(e.message.startsWith(msg)).toBe(true)
    }
  }
})

test('wordhandler create word without props', () => {
  const rdp = new RuDictLineParser()
  rdp.dispatch('где  wo  Ip')
  const dict = rdp.finalize()

  const q = { key: 'где' }
  expect(dict.has(q)).toBe(true)

  const v = dict.get(q)
  expect(v.ru.content).toBe('где')
  expect(v.translation('de')).toBe('wo')
})

test('wordhandler create word with forms', () => {
  const rdp = new RuDictLineParser()
  rdp.dispatch('нас  uns  Pers  1-pl genitive')
  const dict = rdp.finalize()

  const q = { key: 'нас' }
  expect(dict.has(q)).toBe(true)
  const v = dict.get(q)

  expect(v).toBeInstanceOf(PersonalPronoun)
  expect(v.props.person).toBe(props.Plural1st)
  expect(v.props.case).toBe(props.Genitive)
})

test('wordhandler create word with extras', () => {
  const rdp = new RuDictLineParser()
  rdp.dispatch("мо'я  meine  Poss  1-sing f see:мой see:моё comment:example")
  const dict = rdp.finalize()

  const q = { key: 'моя' + CombiningAcuteAccent }
  expect(dict.has(q)).toBe(true)
  const v = dict.get(q)

  expect(v.extra.see).toEqual(['мой', 'моё'])
  expect(v.extra.comment).toBe('example')
})

test('formHandler create and add words with forms', () => {
  const rdp = new RuDictLineParser()
  rdp.dispatch("- зн'аю  знать  1-sing")
  rdp.dispatch('знать  wissen  V  imperf inf')
  rdp.dispatch("- зн'аешь  знать  2-sing")

  const dict = rdp.finalize()

  expect(dict.length).toBe(1)
  const v = dict.get({ key: 'знать' })

  expect(v.forms.length).toBe(2)
  const v1 = v.forms[0]
  const v2 = v.forms[1]
  expect(v1.ru.accented).toBe('зна' + CombiningAcuteAccent + 'ю')
  expect(v1.props.person).toBe(props.Singular1st)
  expect(v1.props.aspect).toBe(props.Imperfective)
  expect(v2.ru.accented).toBe('зна' + CombiningAcuteAccent + 'ешь')
  expect(v2.props.person).toBe(props.Singular2nd)
})

test('wordhandler create word with see and back ref', () => {
  const rdp = new RuDictLineParser()
  rdp.dispatch("мо'я  meine  Poss  1-sing f see:мой")
  rdp.dispatch('мой  mein  Poss  1-sing m')
  const dict = rdp.finalize()

  const v = dict.get({ key: 'мой' })
  expect(v.extra.see).toEqual(['моя' + CombiningAcuteAccent])
})

test('keyValuesHandler', () => {
  const rdp = new RuDictLineParser()
  expect(rdp.ctx.extra).toEqual({})
  rdp.dispatch('date:2020-02-02')
  expect(rdp.ctx.extra).toEqual({ date: '2020-02-02' })
  rdp.dispatch('origin:unknown')
  expect(rdp.ctx.extra).toEqual({ date: '2020-02-02', origin: 'unknown' })
  rdp.dispatch('date:')
  expect(rdp.ctx.extra).toEqual({ origin: 'unknown' })
})
