/* globals expect test */

import { CombiningAcuteAccent as caa, InvalidAccentError, RuTerm, SurplusAccentError, TermList } from '../src/term.js'

test('RuTerm.content and RuTerm.accented', () => {
  const term = RuTerm.parse("зим'а")
  expect(term.content).toBe('зима')
  expect(term.accented).toBe('зима' + caa)
})

test('RuTerm.parse pass with word without vowel', () => {
  const term = RuTerm.parse('в')
  expect(term.accented).toEqual('в')
})

test('RuTerm.parse pass with one-syllable word', () => {
  const term = RuTerm.parse('да')
  expect(term.accented).toEqual('да')
})

test('RuTerm.parse pass with two-syllable word', () => {
  const term = RuTerm.parse("м'ама")
  expect(term.accented).toEqual('ма' + caa + 'ма')
})

test('RuTerm.parse pass with two-syllable word without accent', () => {
  const term = RuTerm.parse('мама')
  expect(term.accented).toEqual('мама')
})

test('RuTerm.parse pass with one-syllable word containing JO', () => {
  const term = RuTerm.parse('всё')
  expect(term.accented).toEqual('всё')
})

test('RuTerm.parse pass with two-syllable word containing JO', () => {
  const term = RuTerm.parse('ёе')
  expect(term.accented).toEqual('ёе')
})

test('RuTerm.parse pass with multiple words', () => {
  const term = RuTerm.parse("д'оброе 'утро")
  expect(term.content).toEqual('доброе утро')
  expect(term.accented).toEqual('до' + caa + 'брое у' + caa + 'тро')
})

test('RuTerm.parse pass with multiple words and missing accent in 2nd word "', () => {
  const term = RuTerm.parse("д'оброе утро")
  expect(term.content).toEqual('доброе утро')
  expect(term.accented).toEqual('до' + caa + 'брое утро')
})

test('RuTerm.parse fail with one-syllable word with accent', () => {
  const t = () => {
    RuTerm.parse("д'а")
  }
  expect(t).toThrow(SurplusAccentError)
})

test('RuTerm.parse fail with word having accent before consonant', () => {
  const t = () => {
    RuTerm.parse("ма'ма")
  }
  expect(t).toThrow(InvalidAccentError)
})

test('RuTerm.parse fail with two-syllable word two accents', () => {
  const t = () => {
    RuTerm.parse("м'ам'а")
  }
  expect(t).toThrow(SurplusAccentError)
})

test('RuTerm.parse fail with word with trailing accent', () => {
  const t = () => {
    RuTerm.parse("мама'")
  }
  expect(t).toThrow(InvalidAccentError)
})

test('RuTerm.parse fail with JO and accent I', () => {
  const t = () => {
    RuTerm.parse("'ёе")
  }
  expect(t).toThrow(SurplusAccentError)
})

test('RuTerm.parse fail with JO and accent II', () => {
  const t = () => {
    RuTerm.parse("ё'е")
  }
  expect(t).toThrow(SurplusAccentError)
})

test('RuTerm.stressIndex', () => {
  const tests = {
    актёр: -1,
    в: -1,
    "велосип'ед": 7,
    дом: -1,
    "м'ама": 1,
    мама: -1
    // "д'обрый день": null
  }
  for (const [str, ix] of Object.entries(tests)) {
    const term = RuTerm.parse(str)
    expect(term.stressIndex).toBe(ix)
  }
})

test('RuTerm.toJSON', () => {
  const term = RuTerm.parse("м'ама")
  expect(term.toJSON()).toEqual({ accented: 'ма' + caa + 'ма', content: 'мама' })
})

test('RuTerm.atomic', () => {
  const term = RuTerm.parse("м'ама")
  expect(term.atomic).toBe(true)
})

test('RuPhrase.toJSON', () => {
  const term = RuTerm.parse("д'оброе 'утро")
  expect(term.toJSON()).toEqual({ accented: 'до' + caa + 'брое у' + caa + 'тро', content: 'доброе утро' })
})

test('RuPhrase.atomic', () => {
  const term = RuTerm.parse("д'оброе 'утро")
  expect(term.atomic).toBe(false)
})

test('TermList.length', () => {
  const list = new TermList('en')
  expect(list.length).toBe(0)

  list.add('house')
  expect(list.length).toBe(1)
})

test('TermList.join', () => {
  const list = new TermList('en')
  list.add('house')
  list.add('home')
  expect(list.join('/')).toBe('house/home')
})

test('Term.toJSON', () => {
  const list = new TermList('en')
  list.add('house')
  list.add('home')

  expect(list.toJSON()).toEqual({ content: ['house', 'home'] })
})
