/* globals expect test */

import 'regenerator-runtime/runtime'

import { LineParser, ParseError } from '../src/parser.js'
import { RuTerm } from '../src/term.js'
import { Vocable } from '../src/vocable.js'

test('line parser', () => {
  let glob = ''
  const twoWordHandler = (ctx, line, w1, w2) => {
    glob = `${w2} ${w1}`
  }

  const lp = new LineParser()
  lp.register(/^([a-z]+) ([a-z]+)$/i, twoWordHandler)

  lp.dispatch('World Hello')
  expect(glob).toBe('Hello World')
})

test('line parser with prepare', () => {
  let glob = ''
  const twoWordHandler = (ctx, line, w1, w2) => {
    glob = `${w2} ${w1}`
  }

  const lp = new LineParser()
  lp.register(/^([a-z]+) ([a-z]+)$/i, twoWordHandler)
  lp.prepare = (line) => {
    return line.replace(/\s+#.+$/, '')
  }

  lp.dispatch('World Hello # a comment')
  expect(glob).toBe('Hello World')
})

test('line parser wich context', () => {
  let glob = ''

  const lp = new LineParser()
  lp.register(/^prefix=([a-z]+)$/i, (ctx, line, w) => { ctx.prefix = w })
  lp.register(/^[a-z]+$/i, (ctx, line) => { glob = `${ctx.prefix} ${line}` })

  lp.dispatch('prefix=Hello')
  lp.dispatch('World')
  expect(glob).toBe('Hello World')
})

test('line parser fail', () => {
  const aHandler = (ctx) => {}

  const lp = new LineParser()
  lp.register(/^[a-z]+$/i, aHandler)

  expect.assertions(2)

  try {
    lp.dispatch('-100')
  } catch (e) {
    expect(e).toBeInstanceOf(ParseError)
    expect(e).toHaveProperty('input', '-100')
  }
})

test('dict parser finalize', () => {
  const aHandler = (ctx, str) => { ctx.dict.add(new Vocable(RuTerm.parse(str))) }
  const lp = new LineParser()
  lp.register(/^(.+)$/i, aHandler)

  lp.dispatch("к'ошка")
  lp.dispatch("соб'ака")
  const dict = lp.finalize()

  expect(dict.length).toBe(2)
})
