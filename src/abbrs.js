import props from './props.js'

import {
  Adjective,
  Adverb,
  Conjunction,
  DemonstrativePronoun,
  Interrogative,
  Name,
  Noun,
  Numeral,
  PersonalPronoun,
  Phrase,
  PossessivePronoun,
  Preposition,
  Verb,
  Vocable
} from './vocable.js'

const posAbbrs = {
  Adj: Adjective,
  Adv: Adverb,
  Cj: Conjunction,
  Dp: DemonstrativePronoun,
  Ip: Interrogative,
  N: Noun,
  Name: Name,
  Num: Numeral,
  Pers: PersonalPronoun,
  Ph: Phrase,
  Poss: PossessivePronoun,
  Prep: Preposition,
  V: Verb,
  Voc: Vocable
}

const posNameAbbrs = {}
for (const [abbr, klass] of Object.entries(posAbbrs)) {
  posNameAbbrs[abbr] = klass.name
}

const propAbbrs = {
  '1-pl': props.Plural1st,
  '1-sing': props.Singular1st,
  '2-pl': props.Plural2nd,
  '2-sing': props.Singular2nd,
  '3-pl': props.Plural3rd,
  '3-sing': props.Singular3rd,
  '3-sing-f': props.Singular3rdF,
  '3-sing-m': props.Singular3rdM,
  '3-sing-n': props.Singular3rdN,
  accusative: props.Accusative,
  anim: props.Animate,
  cond: props.Subjunctive,
  dative: props.Dative,
  f: props.Feminine,
  future: props.Future,
  genitive: props.Genitive,
  honor: props.Honorific,
  inf: props.Infinitive,
  'imp!': props.Imperative,
  imperf: props.Imperfective,
  inan: props.Inanimate,
  ind: props.Indicative,
  instrumental: props.Instrumental,
  m: props.Masculine,
  n: props.Neuter,
  nominative: props.Nominative,
  perf: props.Perfective,
  pl: props.Plural,
  prepositional: props.Prepositional,
  sing: props.Singular
}

export { posAbbrs, posNameAbbrs, propAbbrs }
