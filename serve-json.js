import http from 'http'
import { URL } from 'url'

import parseRuDictFile from './src/parsefile.js'
import { queryFilter } from './src/filter.js'

const host = 'localhost'
const port = 8003
const dictPath = 'words.txt'

const requestListener = async (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.writeHead(200)

  let dict = await parseRuDictFile(dictPath)
  console.log('imported', dict.length, 'words')
  console.log('last word is', dict.last.ru.content)

  const url = new URL(req.url, `http://${host}:${port}/`)
  const filter = queryFilter(url.searchParams)
  dict = dict.filter(filter)

  console.log('filtered', dict.length, 'word(s)')

  res.end(JSON.stringify(dict))
}

const server = http.createServer(requestListener)
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`)
})
