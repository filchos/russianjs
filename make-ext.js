import fetch from 'node-fetch'
import fs from 'fs'
import { JSDOM } from 'jsdom'

import parseRuDictFile from './src/parsefile.js'

const WIKTIONARY_BASE = 'https://en.wiktionary.org'
const DICT_PATH = 'words.txt'

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

const getWiktionaryAudioLinks = async (url) => {
  const res = await fetch(url)
  if (!res.ok) {
    console.warn('Status', res.status)
    return null
  }
  const body = await res.text()
  const dom = new JSDOM(body)
  const doc = dom.window.document
  const audioLinks = doc.querySelectorAll('audio[data-mwtitle^="Ru-"] > source[type^="audio/mpeg"]')
  const urls = Array.from(audioLinks).map(node => node.getAttribute('src'))
  return [...new Set(urls)]
}

const loadStore = (path) => {
  if (!fs.existsSync(path)) {
    return {}
  }
  const rawData = fs.readFileSync(path)
  return JSON.parse(rawData)
}

const saveStore = (path, store) => {
  const data = JSON.stringify(store, null, 2)
  fs.writeFileSync(path, data)
  console.log('Saved to ', path)
}

const main = async () => {
  const path = 'externals.json'
  const store = loadStore(path)
  const dict = await parseRuDictFile(DICT_PATH)
  for (const [key, vocable] of dict) {
    const word = vocable.ru.content
    const url = WIKTIONARY_BASE + '/wiki/' + encodeURIComponent(word)

    if (key in store) {
      console.log('Skipping ', word)
      continue
    }

    const urls = await getWiktionaryAudioLinks(url)

    if (!urls) {
      console.log('No wiktionary page found for ', word)
      continue
    }

    const item = {
      wiktionaryURL: url
    }
    if (urls.length === 0) {
      console.log('Wiktionary page found for ', word, ' but no audio URL(s)')
    } else {
      console.log(urls.length, ' audio URL(s) found for ', word)
      item.wikimediaAudioURLs = urls
    }

    store[key] = item
    await delay(1000)
  }
  saveStore(path, store)
}

main()
