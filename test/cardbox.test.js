/* globals expect test */

import { Box, Collection, LocalStorageDriver } from '../src/cardbox.js'

// see https://stackoverflow.com/questions/32911630/how-do-i-deal-with-localstorage-in-jest-tests
var localStorageMock = (function () {
  var store = {}
  return {
    getItem: function (key) {
      return store[key]
    },
    setItem: function (key, value) {
      store[key] = value.toString()
    },
    clear: function () {
      store = {}
    },
    removeItem: function (key) {
      delete store[key]
    }
  }
})()

Object.defineProperty(window, 'localStorage', { value: localStorageMock })

test('Box basics', () => {
  const box = new Box('test-box', 7)

  expect(box.length).toBe(0)
  expect(box.weightedLength).toBe(0)

  box.add(1)

  expect(box.has(1)).toBe(true)
  expect(box.has(2)).toBe(false)
  expect(box.length).toBe(1)
  expect(box.weightedLength).toBe(7)

  box.add(2)
  box.add(1)

  expect(box.has(1)).toBe(true)
  expect(box.has(2)).toBe(true)
  expect(box.length).toBe(2)
  expect(box.weightedLength).toBe(14)

  box.remove(1)

  expect(box.has(1)).toBe(false)
  expect(box.has(2)).toBe(true)
  expect(box.length).toBe(1)
  expect(box.weightedLength).toBe(7)
})

test('Box next', () => {
  const newBox = new Box('new')
  const passBox = new Box('pass')
  const failBox = new Box('fail')
  newBox.addRule('pass', passBox)
  newBox.addRule('fail', failBox)

  newBox.add(97)
  newBox.next('pass', 97)

  expect(newBox.has(97)).toBe(false)
  expect(passBox.has(97)).toBe(true)
})

test('Box persistence', () => {
  window.localStorage.clear()
  const driver = new LocalStorageDriver('test-')

  const collectionv1 = new Collection(driver)
  const box1v1 = new Box('test-box')
  collectionv1.add(box1v1)
  box1v1.add(23)
  box1v1.add(42)
  box1v1.save()

  const collectionv2 = new Collection(driver)
  const box1v2 = new Box('test-box')
  collectionv2.add(box1v2)
  box1v2.load()

  expect(collectionv2.length).toBe(2)
  expect(box1v2.length).toBe(2)
  expect(box1v2.has(24)).toBe(false)
  expect(box1v2.has(42)).toBe(true)

  const box2 = new Box('another-box')
  collectionv2.add(box2)
  box2.add(100)
  box2.save()

  expect(collectionv2.length).toBe(3)

  const collectionv3 = new Collection(driver)
  collectionv3.add(new Box('test-box'))
  collectionv3.add(new Box('another-box'))
  collectionv3.load()

  expect(collectionv3.length).toBe(3)
})

test('Collection basics', () => {
  const box1 = new Box('w-3', 3)
  box1.add(1)
  box1.add(2)
  const box2 = new Box('w-5', 5)
  box2.add(3)

  const collection = new Collection()

  collection.add(box1)

  expect(collection.length).toBe(2)
  expect(collection.weightedLength).toBe(2 * 3)

  collection.add(box2)

  expect(Object.values(collection.boxes).length).toBe(2)
  expect(collection.length).toBe(3)
  expect(collection.weightedLength).toBe(2 * 3 + 5)

  expect(collection.keys.size).toBe(3)
  expect(collection.keys.has(1)).toBe(true)
  expect(collection.keys.has(3)).toBe(true)
  expect(collection.keys.has(4)).toBe(false)

  const wiTests = [
    { ix: 0, box: 'w-3', key: 1 },
    { ix: 2, box: 'w-3', key: 1 },
    { ix: 3, box: 'w-3', key: 2 },
    { ix: 5, box: 'w-3', key: 2 },
    { ix: 6, box: 'w-5', key: 3 },
    { ix: 10, box: 'w-5', key: 3 }
  ]

  for (const t of wiTests) {
    const r = collection.pickByWeightedIndex(t.ix)
    expect(r.boxName).toBe(t.box)
    expect(r.key).toBe(t.key)
  }
  expect(collection.pickByWeightedIndex(11)).toBe(null)
})
