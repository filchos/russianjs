/* globals expect test */

import props from '../src/props.js'
import { RuleSet, NounFormGuess } from '../src/guess.js'
import { RuTerm } from '../src/term.js'
import { Noun } from '../src/vocable.js'

test('RuleSet empty', () => {
  const is = RuleSet.make().resolve('ABC')
  expect(is).toBe(null)
})

test('RuleSet invalid on combination', () => {
  let t = () => {
    RuleSet.make().on(/a/).on(/b/)
  }
  expect(t).toThrow(Error)

  t = () => {
    RuleSet.make().return('A')
  }
  expect(t).toThrow(Error)

  t = () => {
    RuleSet.make().on(/a/).resolve('ABC')
  }
  expect(t).toThrow(Error)
})

test('NounFormGuess.gender success', () => {
  const tests = {
    "вод'а": props.Feminine,
    дом: props.Masculine,
    "р'адио": props.Neuter,
    "рюкз'ак": props.Masculine
  }

  for (const [term, expected] of Object.entries(tests)) {
    const voc = new Noun(RuTerm.parse(term))
    const guess = new NounFormGuess(voc)
    expect(guess.gender).toBe(expected)
  }
})

test('NounFormGuess.gender no basic form', () => {
  const tests = [
    new Noun(RuTerm.parse('дом'), { animacy: props.Animate }),
    new Noun(RuTerm.parse('дом'), { case: props.Genitive }),
    new Noun(RuTerm.parse('дом'), { number: props.Plural })
  ]

  for (const voc of tests) {
    const guess = new NounFormGuess(voc)
    expect(guess.gender).toBe(false)
  }
})

test('NounFormGuess.plural', () => {
  const tests = [
    // with defined gender
    ["тар'елка", props.Feminine, 'тарелки'],
    ['план', props.Masculine, 'планы'],
    ["яйц'о", props.Neuter, 'яйца'],
    // without defined gender
    ["тар'елка", null, 'тарелки'],
    ['план', null, 'планы'],
    ["яйц'о", null, 'яйца']
  ]

  for (const [term, gender, expected] of tests) {
    const voc = new Noun(RuTerm.parse(term), { gender })
    const guess = new NounFormGuess(voc)
    expect(guess.plural).toBe(expected)
  }
})

// // http://russian.cornell.edu/verbs/irg06stemchanges.htm
// test('Verb.deklination', () => {
//   // знать — “to know”
//   const tests = [
//     [props.Singular1st, 'знаю'],
//     [props.Singular2nd, 'знаешь'],
//     [props.Singular3rd, 'знает'],
//     [props.Plural1st, 'знаем'],
//     [props.Plural2nd, 'знаете'],
//     [props.Plural3rd, 'знают']
//   ]

//   const voc = new Verb(RuTerm.parse('зна', { conjugation: 'e', vform: props.nonPastStem }))
//   for (const [form, expected] of tests) {
//     const guess = new VerbRule(voc, form).decline(form)
//     expect(guess).toBe(expected)
//   }
// })
