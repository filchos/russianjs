import props from './props.js'
import { TermList } from './term.js'

class Vocable {
  constructor (term, propArgs = {}, defaultProps = null) {
    this.ru = term
    this.props = {}
    this.terms = {}
    this.extra = {}
    this.forms = []
    this.examples = []

    if (!defaultProps) {
      defaultProps = this.constructor.props
    }

    for (const [propKey, defaultProp] of Object.entries(defaultProps)) {
      this.props[propKey] = propArgs[propKey] ?? defaultProp
    }
  }

  get pos () { // part of speech
    return this.constructor.name
  }

  get key () {
    let k = this.ru.accented
    if (this.extra.variant) {
      k += '/' + this.extra.variant
    }
    return k
  }

  get valid () {
    return true
  }

  addTranslation (code, str) {
    if (!this.terms[code]) {
      this.terms[code] = new TermList(code)
    }
    this.terms[code].add(str)
    return this
  }

  translations (code) {
    if (this.terms[code]) {
      return this.terms[code].content
    } else {
      return []
    }
  }

  translation (code) {
    return this.translations(code).join(', ')
  }

  inheritPropsFromParent (parentVoc) {

  }

  toJSON () {
    const json = {}
    json.ru = this.ru
    json.pos = this.pos
    if (Object.keys(this.props).length > 0) {
      json.props = this.props
    }
    if (Object.keys(this.terms).length > 0) {
      json.terms = this.terms
    }
    const extra = { ...this.extra }
    delete extra.origin
    delete extra.date
    if (Object.keys(extra).length > 0) {
      json.extra = extra
    }
    if (this.forms.length) {
      json.forms = this.forms
    }
    if (this.examples.length) {
      json.examples = this.examples
    }
    return json
  }
}

Vocable.props = {}

// subclasses

class Adjective extends Vocable {}

Adjective.props = {
  case: props.Nominative,
  gender: props.Masculine,
  number: props.Singular
}

class Adverb extends Vocable {}

Adverb.props = {}

class Conjunction extends Vocable {}

Conjunction.props = {}

class DemonstrativePronoun extends Vocable {}

DemonstrativePronoun.props = {
  case: props.Nominative,
  gender: null,
  number: props.Singular
}

class Interrogative extends Vocable {}

Interrogative.props = {}

class PersonalPronoun extends Vocable {}

PersonalPronoun.props = {
  case: props.Nominative,
  honorific: false,
  person: null
}

class Name extends Vocable {}

Name.props = {
  case: props.Nominative,
  gender: null
}

class Noun extends Vocable {
  inheritPropsFromParent (parentVoc) {
    this.props.animacy = parentVoc.props.animacy
    this.props.gender = parentVoc.props.gender
  }
}

Noun.props = {
  animacy: props.Inanimate,
  case: props.Nominative,
  gender: null,
  number: props.Singular
}

class Numeral extends Vocable {}

Numeral.props = {}

class Phrase extends Vocable {}

Phrase.props = {}

class PossessivePronoun extends Vocable {}

PossessivePronoun.props = {
  honorific: false,
  case: props.Nominative,
  gender: null, // object
  number: null, // object
  person: null
}

class Preposition extends Vocable {}

Preposition.props = {}

class Verb extends Vocable {
  constructor (term, propArgs = {}) {
    if (propArgs.vform === props.Infinitive) {
      super(term, propArgs, { aspect: props.Imperfective, vform: null })
    } else {
      super(term, propArgs)
    }
  }

  inheritPropsFromParent (parentVoc) {
    this.props.aspect = parentVoc.props.aspect
  }
}

Verb.props = {
  aspect: props.Imperfective,
  mood: props.Indicative,
  person: null,
  tense: props.Present,
  vform: props.Finite
}

export {
  Adjective,
  Adverb,
  Conjunction,
  DemonstrativePronoun,
  Interrogative,
  Name,
  Noun,
  Numeral,
  PersonalPronoun,
  Phrase,
  PossessivePronoun,
  Preposition,
  Verb,
  Vocable
}
