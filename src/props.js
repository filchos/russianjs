export default {
  // animacy
  Animate: 'animate',
  Inanimate: 'inanimate',
  // aspect
  Imperfective: 'imperfective',
  Perfective: 'perfective',
  // case
  Nominative: 'nominative',
  Accusative: 'accusative',
  Dative: 'dative',
  Genitive: 'genitive',
  Instrumental: 'instrumental',
  Prepositional: 'prepositional',
  // gender
  Feminine: 'feminine',
  Masculine: 'masculine',
  Neuter: 'neuter',
  // mood
  Indicative: 'indicative',
  Imperative: 'imperative',
  Subjunctive: 'subjunctive',
  // number
  Singular: 'singular',
  Plural: 'plural',
  // person
  Singular1st: '1st person singular',
  Singular2nd: '2nd person singular',
  Singular3rd: '3rd person singular',
  Singular3rdM: '3rd person singular masculine',
  Singular3rdF: '3rd person singular feminine',
  Singular3rdN: '3rd person singular neuter',
  Plural1st: '1st person plural',
  Plural2nd: '2nd person plural',
  Plural3rd: '3rd person plural',
  // tense
  Future: 'future',
  Present: 'present',
  // misc. flags
  Honorific: 'honorific',
  Finite: 'finite',
  Infinitive: 'infinitive',
  Stem: 'stem'
}
