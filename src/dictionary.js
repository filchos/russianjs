import ExtendableError from './error.js'

class KeyConflictError extends ExtendableError {}

const cmp = ([one], [other]) => {
  one = one.toLowerCase()
  other = other.toLowerCase()
  if (one > other) return 1
  if (one < other) return -1
  return 0
}

class Dictionary {
  constructor () {
    this.vocables = {}
    this.last = null
  }

  * [Symbol.iterator] () {
    for (const item of Object.entries(this.vocables)) {
      yield [item[0], item[1]]
    }
  }

  add (vocable) {
    const key = vocable.key
    if (this.has(vocable)) {
      throw new KeyConflictError(vocable.key)
    }
    this.vocables[key] = vocable
    this.last = vocable
  }

  get (vocable) {
    return this.vocables[vocable.key] || null
  }

  has (vocable) {
    return vocable.key in this.vocables
  }

  filter (callback) {
    const dict = new Dictionary()
    for (const [, vocable] of this) {
      if (callback(vocable)) {
        dict.add(vocable)
      }
    }
    return dict
  }

  get length () {
    return Object.keys(this.vocables).length
  }

  get keys () {
    return Object.keys(this.vocables)
  }

  sortByKey () {
    const sorted = Object.fromEntries(
      Object.entries(this.vocables).sort(cmp)
    )
    this.vocables = sorted
  }

  toJSON () {
    return this.vocables
  }
}

export { KeyConflictError, Dictionary }
