/* globals expect test */

import { isConsonant, isHardVowel, isSign, isSoftVowel, isVowel } from '../src/letterclass.js'

test('isConsonant', () => {
  expect(isConsonant('Ё')).toBe(false)
  expect(isConsonant('ы')).toBe(false)
  expect(isConsonant('Ш')).toBe(true)
  expect(isConsonant('ж')).toBe(true)
  expect(isConsonant('ь')).toBe(false)
})

test('isSign', () => {
  expect(isSign('Ё')).toBe(false)
  expect(isSign('ы')).toBe(false)
  expect(isSign('Ш')).toBe(false)
  expect(isSign('ж')).toBe(false)
  expect(isSign('ь')).toBe(true)
})

test('isVowel', () => {
  expect(isVowel('Ё')).toBe(true)
  expect(isVowel('ы')).toBe(true)
  expect(isVowel('Ш')).toBe(false)
  expect(isVowel('ж')).toBe(false)
  expect(isVowel('ь')).toBe(false)
})

test('isSoftVowel', () => {
  expect(isSoftVowel('Ё')).toBe(true)
  expect(isSoftVowel('ы')).toBe(false)
})

test('isHardVowel', () => {
  expect(isHardVowel('Ё')).toBe(false)
  expect(isHardVowel('ы')).toBe(true)
})
