/* globals expect test */

import { everyFilter } from '../src/filter.js'

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

test('EveryFilterwith 0 filters', () => {
  const f = everyFilter()
  const expected = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  const is = numbers.filter(f)
  expect(is).toEqual(expected)
})

test('EveryFilter with 1 filter', () => {
  const f1 = (x) => x % 2 === 1
  const f = everyFilter(f1)
  const expected = [1, 3, 5, 7, 9]

  const is = numbers.filter(f)
  expect(is).toEqual(expected)
})

test('EveryFilter with 2 filters', () => {
  const f1 = (x) => x % 2 === 1
  const f2 = (x) => x % 3 === 1
  const f = everyFilter(f1, f2)
  const expected = [1, 7]

  const is = numbers.filter(f)
  expect(is).toEqual(expected)
})
