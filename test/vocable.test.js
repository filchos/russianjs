/* globals expect test */

import props from '../src/props.js'
import { RuTerm } from '../src/term.js'
import { Noun, Vocable } from '../src/vocable.js'

test('new Vocable', () => {
  const term = RuTerm.parse('да')
  const v = new Vocable(term)
  expect(v.ru.content).toBe('да')
})

test('Vocable.key', () => {
  const term = RuTerm.parse('да')
  const v = new Vocable(term)

  expect(v.key).toBe('да')

  v.extra.variant = 'v1'

  expect(v.key).toBe('да/v1')
})

test('Vocable.valid', () => {
  const term = RuTerm.parse('да')
  const v = new Vocable(term)

  expect(v.valid).toBe(true)
})

test('Vocable.addTranslation, Vocable.translation*', () => {
  const term = RuTerm.parse('дом')
  const v = new Vocable(term)

  expect(v.translations('de')).toStrictEqual([])
  expect(v.translation('de')).toBe('')

  v.addTranslation('de', 'Haus')

  expect(v.translation('de')).toEqual('Haus')
  expect(v.translations('de')).toEqual(['Haus'])

  v.addTranslation('de', 'Heim')
  v.addTranslation('en', 'House')

  expect(v.translations('de')).toEqual(['Haus', 'Heim'])
  expect(v.translations('en')).toEqual(['House'])
  expect(v.translation('de')).toBe('Haus, Heim')
  expect(v.translation('en')).toBe('House')
})

test('Vocable.toJSON', () => {
  const voc = new Noun(RuTerm.parse('дом'), { gender: props.Masculine })
  voc.addTranslation('de', 'Haus')
  voc.extra.comment = 'Example word'

  const ruJSON = JSON.stringify(voc.ru)
  const expStr = `
    {
      "extra": {
        "comment": "Example word"
      },
      "pos":"Noun",
      "props": {
        "animacy": "inanimate",
        "case": "nominative",
        "gender": "masculine",
        "number": "singular"
      },
      "ru":${ruJSON},
      "terms":{
        "de":{
          "content":["Haus"]
        }
      }
    }
  `

  const expected = JSON.parse(expStr)
  const is = JSON.parse(JSON.stringify(voc))
  expect(is).toStrictEqual(expected)
})
