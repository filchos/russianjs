import { Dictionary } from './dictionary.js'

const MsgNoMatch = 'no matching handler found'

class ParseError extends Error {
  constructor (line, msg) {
    super(`${msg} in "${line}"`)
    this.input = line
  }
}

class LineParser {
  constructor () {
    this.ctx = {
      dict: new Dictionary()
    }
    this.handlers = new Map()
  }

  prepare (line) {
    return line
  }

  register (pattern, handler) {
    this.handlers.set(pattern, handler)
  }

  dispatch (line) {
    const preparedLine = this.prepare(line)
    for (const [pattern, handler] of this.handlers) {
      const matches = preparedLine.match(pattern)
      if (matches) {
        const args = matches
        handler(this.ctx, ...args)
        return
      }
    }
    throw new ParseError(line, MsgNoMatch)
  }

  finalize () {
    return this.ctx.dict
  }
}

export {
  LineParser,
  MsgNoMatch,
  ParseError
}
