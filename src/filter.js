import { posNameAbbrs, propAbbrs } from './abbrs.js'
import smartFilter from './smartfilter.js'

const everyFilter = (...filters) => {
  return (item) => {
    for (const filter of filters) {
      if (!filter(item)) {
        return false
      }
    }
    return true
  }
}

const queryFilter = (searchParams) => {
  const extras = ['comment']
  const extraArrays = ['level', 'see']
  const props = ['animacy', 'aspect', 'case', 'gender', 'honorific', 'mood', 'number', 'person', 'vform']

  const filters = []

  const pos = searchParams.get('pos')
  if (pos) {
    const f = smartFilter(pos, posNameAbbrs)
    filters.push(voc => f.matches(voc.pos))
  }

  const ruWord = searchParams.get('ru')
  if (ruWord) {
    const f = smartFilter(ruWord)
    filters.push(voc => f.matches(voc.ru.content))
  }

  for (const prop of props) {
    const pattern = searchParams.get(prop)
    if (pattern) {
      const f = smartFilter(pattern, propAbbrs)
      filters.push(voc => f.matches(voc.props[prop]))
    }
  }

  for (const extra of extras) {
    const pattern = searchParams.get(extra)
    if (pattern) {
      const f = smartFilter(pattern)
      filters.push(voc => f.matches(voc.extra[extra]))
    }
  }

  for (const extra of extraArrays) {
    const pattern = searchParams.get(extra)
    if (pattern) {
      filters.push(voc => (voc.extra[extra].includes(pattern)))
    }
  }

  return everyFilter(...filters)
}

export { everyFilter, queryFilter }
