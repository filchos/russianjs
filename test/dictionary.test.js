/* globals expect test */

import 'regenerator-runtime/runtime'

import { Dictionary, KeyConflictError } from '../src/dictionary.js'
import props from '../src/props.js'
import { RuTerm } from '../src/term.js'
import { Noun, Vocable } from '../src/vocable.js'

test('new Dictionary', () => {
  const dict = new Dictionary()
  expect(dict.length).toBe(0)

  const v = new Noun(RuTerm.parse('дом'))
  dict.add(v)

  expect(dict.length).toBe(1)
})

test('iterate Dictionary', () => {
  const dict = new Dictionary()
  const vocables = [
    new Noun(RuTerm.parse('дом'), { gender: props.Masculine }),
    new Noun(RuTerm.parse("к'омната"), { gender: props.Feminine }),
    new Noun(RuTerm.parse('стул'), { gender: props.Masculine })
  ]
  for (const vocable of vocables) {
    dict.add(vocable)
  }

  let i = 0
  for (const [key, vocable] of dict) {
    expect(key).toBe(vocable.key)
    expect(vocable).toBe(vocables[i])
    i++
  }
})

test('Dictionary.get', () => {
  const dict = new Dictionary()
  const v = new Vocable(RuTerm.parse('дом'))

  const v1 = dict.get(v)
  expect(v1).toBeNull()

  dict.add(v)

  const v2 = dict.get(v)
  expect(v2).toStrictEqual(v)
})

test('Dictionary.has', () => {
  const dict = new Dictionary()
  const v = new Vocable(RuTerm.parse('дом'))

  expect(dict.has(v)).toBe(false)

  dict.add(v)

  expect(dict.has(v)).toBe(true)
})

test('Dictionary fails on adding same key twice', () => {
  const dict = new Dictionary()
  const v = new Vocable(RuTerm.parse('дом'))

  dict.add(v)
  const t = () => {
    dict.add(v)
  }

  expect(t).toThrow(KeyConflictError)
})

test('Dictionary passes on adding same word in two versions', () => {
  const dict = new Dictionary()
  const v1 = new Vocable(RuTerm.parse('есть'))
  v1.extra.variant = 'essen'
  const v2 = new Vocable(RuTerm.parse('есть'))
  v2.extra.variant = 'da ist'

  dict.add(v1)
  dict.add(v2)

  expect(dict.length).toBe(2)
})

test('filter', () => {
  const dict = new Dictionary()

  const vocables = [
    new Noun(RuTerm.parse('дом'), { gender: props.Masculine }),
    new Noun(RuTerm.parse("к'омната"), { gender: props.Feminine }),
    new Noun(RuTerm.parse('стул'), { gender: props.Masculine })
  ]

  for (const vocable of vocables) {
    dict.add(vocable)
  }

  const f = (v) => v.props.gender === props.Masculine
  const mDict = dict.filter(f)

  expect(mDict.length).toBe(2)
  expect(mDict.has(vocables[0])).toBe(true)
  expect(mDict.has(vocables[1])).toBe(false)
  expect(mDict.has(vocables[2])).toBe(true)
})

test('toJSON', () => {
  const dict = new Dictionary()
  const voc = new Noun(RuTerm.parse('дом'), props.Masculine)
  voc.addTranslation('de', 'Haus')
  dict.add(voc)

  const expected = JSON.parse('{"дом":' + JSON.stringify(voc) + '}')
  const is = JSON.parse(JSON.stringify(dict))
  expect(is).toStrictEqual(expected)
})
